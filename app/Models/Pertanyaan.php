<?php

namespace App\Models;

use App\Http\Controllers\CommentarController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table = 'post';
    protected $fillable = ['title', 'post', 'asset', 'category_id'];
    use HasFactory;

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function comment()
    {
        return $this->hasMany(Comments::class, 'post_id');
    }

}
