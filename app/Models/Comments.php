<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    use HasFactory;
    protected $table = 'reply';
    protected $fillable = ['user_id', 'post_id', 'replies'];

    public function pertanyaan()
    {
        return $this->belongsTo(Pertanyaan::class, 'post_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }   
}
