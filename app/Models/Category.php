<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';
    protected $fillable = ['name', 'description'];
    
    use HasFactory;

    public function pertanyaan()
    {
        return $this->hasMany(Pertanyaan::class, 'category_id');
    }

    public function post()
    {
        return $this->hasMany(Pertanyaan::class, 'category_id');
    }
}
