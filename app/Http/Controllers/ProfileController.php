<?php

namespace App\Http\Controllers;

use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Profile;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    public function profile()
    {
        $idUser = Auth::id();

        $profile = Profile::where('user_id', $idUser)->first();

        return view('profile.edit', ['profile' => $profile]);
    }

    public function update($id, Request $request)
    {
        //dd($request->all());
        $request->validate([
            'name' => 'required',
            'age' => 'required',
            'address' => 'required',
            'bio' => 'required'
        ]);

        Profile::where('id', $id)
        ->update(
            [
                'name' => $request['name'],
                'age' => $request['age'],
                'address' => $request['address'],
                'bio' => $request['bio'],

            ]
        );

        Alert::success('Berhasil!', 'Berhasil mengubah profil Anda. Silakan periksa "My Profile" untuk melihat perubahan.');
        return redirect('/home');

    }


}
