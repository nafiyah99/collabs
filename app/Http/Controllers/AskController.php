<?php

namespace App\Http\Controllers;

use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Category;

class AskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index');

    }

    public function create()
    {
        return view('category.create');
    }

    public function home() 
    {
        return view('layout.home');

    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ],
        [
            'name.required' => "Masukkan kategori yang ingin anda tambahkan, setelah itu klik Kirim"
        ]);

        DB::table('category')->insert([
            'name' => $request['name'],
            'description' => $request['description']
        ]);

        Alert::success('Berhasil!', 'Berhasil menambah kategori baru.');
        return redirect('/ask');
    }

    public function index()
    {
        $category = DB::table('category')->get();
        
        // dd($category);

        return view('category.tampil', ['category' => $category]);
    }

    public function show($id)
    {
        $categories = Category::find($id);

        return view('category.detail', ['categories' => $categories]);
    }

    public function edit($id)
    {
        $categories = DB::table('category')->find($id);
        return view('category.edit', ['categories' => $categories]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ],
        [
            'name.required' => "Masukkan kategori yang ingin anda tambahkan, setelah itu klik Kirim"
        ]);

        DB::table('category')
            ->where('id', $id)
            ->update(
                [
                    'name' => $request['name'],
                    'description' => $request['description']
                ]
            );

        Alert::success('Berhasil!', 'Berhasil mengubah kategori.');
        return redirect('/ask');
    }

    public function destroy($id)
    {
        DB::table('category')->where('id', '=', $id)->delete();

        Alert::success('Berhasil!', 'Berhasil menghapus kategori.');
        return redirect('/ask');
    }
}
