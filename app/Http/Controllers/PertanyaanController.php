<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Pertanyaan;
use File;

class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Pertanyaan::all();
        return view('pertanyaan.tampil',['post' => $post]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        return view('pertanyaan.tambah', ['category'=> $category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'title' => 'required',
            'post' => 'required',
            'asset' => 'required|mimes:png,jpg,jpeg|max:2048',
            'category_id' => 'required',
    
            ]);

            // Konversi nama file gambar
            $imageName = time().'.'.$request->asset->extension();  

            // File gambar disimpan di folder public/asset
            $request->asset->move(public_path('image'), $imageName);

            // Insert data ke dalam database
            $post = new Pertanyaan;
 
            $post->title = $request->title;
            $post->post = $request->post;
            $post->asset = $imageName;
            $post->category_id = $request->category_id;
     
            $post->save();
            
            Alert::success('Berhasil!', 'Berhasil membuat pertanyaan baru.');
            // Kembali ke halaman /pertanyaan
            return redirect('/pertanyaan');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Pertanyaan::find($id);

        return view('pertanyaan.detail', ['post'=>$post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::all();
        $post = Pertanyaan::find($id);

        return view('pertanyaan.edit', ['post'=>$post, 'category'=>$category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'title' => 'required',
            'post' => 'required',
            'asset' => 'mimes:png,jpg,jpeg|max:2048',
            'category_id' => 'required',
    
            ]);

            $post = Pertanyaan::find($id);
 
            $post->title = $request->title;
            $post->post = $request->post;
            $post->category_id = $request->category_id;
 
            
            if ($request->has('asset')) {
                $path = 'image/';
                File::delete($path. $post->asset);

                $imageName = time().'.'.$request->asset->extension();  

                $request->asset->move(public_path('image'), $imageName);

                $post->asset = $imageName;

            }

            $post->save();

            Alert::success('Berhasil!', 'Berhasil mengubah pertanyaan.');
            return redirect('/pertanyaan');
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Pertanyaan::find($id);

        $path = 'image/';
        File::delete($path. $post->asset);

        $post->delete();
        
        Alert::warning('Hapus', 'Berhasil menghapus pertanyaan');
        return redirect('/pertanyaan');
    }
}
