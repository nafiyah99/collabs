<?php

namespace App\Http\Controllers;

use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;
use App\Models\Comments;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class CommentarController extends Controller
{
    public function store($id, Request $request)
    {
        $request->validate([
            'replies' => 'required',
        ]);
        $idUser = Auth::id();

        $comment = new Comments;
 
            $comment->replies = $request->replies;
            $comment->user_id = $idUser;
            $comment->post_id = $id;

            $comment->save();

            Alert::success('Berhasil!', 'Komentar telah berhasil dibuat.');

            return redirect('/pertanyaan/' . $id);
    }

    public function edit($id)
    {
        $categories = DB::table('reply')->find($id);
        return view('pertanyaan.detail', ['reply' => $categories]);
    }
}
