# Final Project (Batch 42)

## Group 10

## Group Members

- Albi Daud
- Nurul Afiyah

## Project Theme

Forum Diskusi

## ERD Design

<img src="public/images/ERD.drawio.png">

## Video Links

Demo Link: https://youtu.be/npaYzWqfjNc

Display Link: http://groupsepuluh.sanbercodeapp.com/
