<!DOCTYPE html>
<html>

<head>
  <!-- Basic -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <!-- Site Metas -->
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <meta name="author" content="" />

  <title>AskMe</title>


  <!-- bootstrap core css -->
  <link rel="stylesheet" type="text/css" href="{{asset('/template/css/bootstrap.css')}}" />

  <!-- fonts style -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:400,600,700&display=swap" rel="stylesheet">

  <!-- font awesome style -->
  <link href="{{asset('/template/css/font-awesome.min.css')}}" rel="stylesheet" />
  <!-- nice select -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/css/nice-select.min.css" integrity="sha256-mLBIhmBvigTFWPSCtvdu6a76T+3Xyt+K571hupeFLg4=" crossorigin="anonymous" />
  <!-- slidck slider -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" integrity="sha256-UK1EiopXIL+KVhfbFa8xrmAWPeBjMVdvYMYkTAEv/HI=" crossorigin="anonymous" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css.map" integrity="undefined" crossorigin="anonymous" />


  <!-- Custom styles for this template -->
  <link href="{{asset('/template/css/style.css')}}" rel="stylesheet" />
  <!-- responsive style -->
  <link href="{{asset('css/responsive.css')}}" rel="stylesheet" />

  <link rel="shortcut icon" href="{{asset('/template_homepage/images/AskMe-icon.png')}}"/>

</head>

<body>

  <div class="hero_area">
    <!-- header section strats -->
    <header class="header_section">
      <div class="container-fluid">
        <nav class="navbar navbar-expand-lg custom_nav-container">
          <a class="navbar-brand" href="">
            <span>
              .AskMe
            </span>
          </a>
          
        </nav>
      </div>
    </header>
    <!-- end header section -->

    <!-- slider section -->
    @guest
    <section class="slider_section ">
      <div class="container ">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <div class="detail-box">
              <h1>
                Global Forum
              </h1>
              <p>
                .AskMe is a forum that can be accessed easily by anyone around the world <br> Fulfill your curiosity through .AskMe!
              </p>
            </div>
            <div class="find_container ">
              <div class="container">
                <div class="heading_container heading_center">
                  
                  @guest
                    
                  <div class="btn-box">
                    <a class="nav-link bg-warning" href="{{ route('login') }}"> {{ __('Login') }}</a>
                  </div>
                    
                  @endguest
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- end slider section -->
    @endguest
  </div>


  <!-- recipe section -->
  
  <section class="recipe_section">
    <div class="container">
      <div class="heading_container heading_center">
        <h2>
          Popular Topics
        </h2>
      </div>
      <div class="row">
        <div class="col-sm-6 col-md-4 mx-auto">
          <div class="box">
            <div class="img-box">
              <img src="{{asset('/template/images/r2.jpg')}}" class="box-img" alt="">
            </div>
            <div class="detail-box">
              <h4>
                Food
              </h4>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 mx-auto">
          <div class="box">
            <div class="img-box">
              <img src="{{asset('/template/images/r4.jpg')}}" class="box-img" alt="">
            </div>
            <div class="detail-box">
              <h4>
                Tour
              </h4>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 mx-auto">
          <div class="box">
            <div class="img-box">
              <img src="{{asset('/template/images/r5.jpg')}}" class="box-img" alt="">
            </div>
            <div class="detail-box">
              <h4>
                Digital Skill
              </h4>
            </div>
          </div>
        </div>
      </div>
      @auth
      <div class="btn-box">
        <a href="/pertanyaan/create">
          Ask Now
        </a>
      </div>
      @endauth
    </div>
  </section>
  
  <!-- end recipe section -->

  <!-- about section -->
  @guest
  <section class="about_section layout_padding">
    <div class="container">
      <div class="col-md-11 col-lg-10 mx-auto">
        <div class="heading_container heading_center">
          <h2>
            About Us
          </h2>
        </div>
        <div class="box">
          <div class="col-md-7 mx-auto">
          </div>
          <div class="detail-box">
            <p>
              .AskMe is a question and answer forum on various topics that users around the world can create and add to. You can ask anything and include pictures to support the questions and answers you ask. With .AskMe, sharing knowledge becomes more fun!
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>
  @endguest

  <!-- end about section -->

  <div class="footer_container">
    <!-- info section -->
    <section class="info_section layout_padding-top">
      <div class="container">
        @guest
        <div class="contact_box">
          <a href="">
            <i class="fa fa-map-marker" aria-hidden="true"></i>
          </a>
          <a href="">
            <i class="fa fa-phone" aria-hidden="true"></i>
          </a>
          <a href="">
            <i class="fa fa-envelope" aria-hidden="true"></i>
          </a>
        </div>
        @endguest
        @auth
        <div class="heading_container heading_center">
        <div class="btn-box">
          <a href="/home">
            Go to Dashboard<br>
            <i class="fa fa-arrow-right" aria-hidden="true"></i>
          </a>
        </div>  
        </div>  
        @endauth
      </div>
    </section>
    <!-- end info_section -->


    <!-- footer section -->
    <footer class="footer_section">
      <div class="container">
        <p>
          &copy; <span id="displayYear"></span> All Rights Reserved
          <br>Developed by Group 10
        </p>
      </div>
    </footer>
    <!-- footer section -->

  </div>
  
  <!-- jQery -->
  <script src="{{asset('/template/js/jquery-3.4.1.min.js')}}"></script>
  <!-- bootstrap js -->
  <script src="{{asset('/template/js/bootstrap.js')}}"></script>
  <!-- slick  slider -->
  <script src="{{asset('/template/https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.js')}}"></script>
  <!-- nice select -->
  <script src="{{asset('/template/https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/js/jquery.nice-select.min.js')}}" integrity="sha256-Zr3vByTlMGQhvMfgkQ5BtWRSKBGa2QlspKYJnkjZTmo=" crossorigin="anonymous"></script>
  <!-- custom js -->
  <script src="{{asset('/template/js/custom.js')}}"></script>


</body>

</html>