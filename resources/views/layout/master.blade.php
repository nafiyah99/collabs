<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>.AskMe</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{asset('/template_homepage/vendors/feather/feather.css')}}"/>
  <link rel="stylesheet" href="{{asset('/template_homepage/vendors/ti-icons/css/themify-icons.css')}}"/>
  <link rel="stylesheet" href="{{asset('/template_homepage/vendors/css/vendor.bundle.base.css')}}"/>
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <link rel="stylesheet" href="{{asset('/template_homepage/vendors/datatables.net-bs4/dataTables.bootstrap4.css')}}"/>
  <link rel="stylesheet" href="{{asset('/template_homepage/vendors/ti-icons/css/themify-icons.css')}}"/>
  <link rel="stylesheet" type="text/css" href="{{asset('/template_homepage/js/select.dataTables.min.css')}}"/>
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{asset('/template_homepage/css/vertical-layout-light/style.css')}}"/>
  <!-- endinject -->
  <link rel="shortcut icon" href="{{asset('/template_homepage/images/AskMe-icon.png')}}"/>
  <script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=63fc2b6210a925001240e419&product=inline-reaction-buttons&source=platform" async="async"></script>
</head>
<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    @include('partials.nav')
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->
      <div class="theme-setting-wrapper">
        
      </div>
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
      @include('partials.sidebar')
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-md-12 grid-margin">
              <div class="row">
                <div class="col-12 col-xl-8 mb-4 mb-xl-0">
                  <h2 class="font-weight-bold">@yield('title')</h2>

                  {{-- <h6 class="font-weight-normal mb-0">Belajar dan berbagi solusi bersama</h6> --}}
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">@yield('content')</h4>
              </div>
            </div>
          </div>
          

          {{-- <div class="card tale-bg grid-margin stretch-card">
            @yield('content_2')
          </div> <br> <br> --}}

        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2023 Laravel Batch 42 by Sanbercode</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="ti-heart text-danger ml-1"></i></span>
          </div>
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Kelompok 10 - Albi Daud & Nurul Afiyah</span> 
          </div>
        </footer> 
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>   
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="{{asset('/template_homepage/vendors/js/vendor.bundle.base.js')}}"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <script src="{{asset('/template_homepage/vendors/chart.js/Chart.min.js')}}"></script>
  <script src="{{asset('/template_homepage/vendors/datatables.net/jquery.dataTables.js')}}"></script>
  <script src="{{asset('/template_homepage/vendors/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>
  <script src="{{asset('/template_homepage/js/dataTables.select.min.js')}}"></script>

  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="{{asset('/template_homepage/js/off-canvas.js')}}"></script>
  <script src="{{asset('/template_homepage/js/hoverable-collapse.js')}}"></script>
  <script src="{{asset('/template_homepage/js/template.js')}}"></script>
  <script src="{{('/template_homepage/js/settings.js')}}"></script>
  <script src="{{asset('/template_homepage/js/todolist.js')}}"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="{{asset('/template_homepage/js/dashboard.js')}}"></script>
  <script src="{{asset('/template_homepage/js/Chart.roundedBarCharts.js')}}"></script>
  <!-- End custom js for this page-->
  @stack('script')
  @include('sweetalert::alert')

</body>

</html>