@extends('layout.master')

@section('title')
    Selamat Datang di .AskMe!
@endsection

@section('content')
    <div class="card-body">
        <h5 class="card-title">Tulis pertanyaanmu di sini!</h5>
        <p class="card-text">Ceritakan kendala yang sedang kamu hadapi</p> <br>
        <a href="/pertanyaan/create" class="btn btn-primary">Tulis pertanyaan</a>
        <a href="/pertanyaan/" class="btn btn-primary">Lihat pertanyaan</a>
    </div>

    
@endsection