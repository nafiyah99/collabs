@extends('layout.master')

@section('title')
    Daftar Kategori
@endsection

@section('content')

<a href="/ask/create" class="btn btn-dark btn-sm my-3">+ Tambahkan Kategori</a>

<table class="table text-center">
    <thead>
        <tr>
            <th scope="col">No</th>
            <th scope="col">Jenis Kategori</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($category as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item -> name}}</td>
                <td>
                    <form action="/ask/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/ask/{{$item->id}}" class="btn btn-info btn-sm">Deskripsi</a>
                        <a href="/ask/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" value="Hapus" class="btn btn-danger btn-sm">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td>Kategori belum ditambahkan</td>
            </tr>
        @endforelse
    </tbody>
</table>

@endsection

