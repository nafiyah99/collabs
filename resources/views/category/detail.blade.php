@extends('layout.master')

@section('title')
    Detail Kategori
@endsection

@section('content')

<h2>{{$categories->name}}</h2>
<br>
<p>{!!$categories->description!!}</p>

<div class="row">
@forelse ($categories->pertanyaan as $item)
    <div class="col-4">
        <div class="card">
            <img class="card-img-top" src="{{asset('/image/' . $item->asset)}}" height="350px" alt="Card image cap">
            <div class="card-body">
            <span class="badge badge-warning">{{$item->category->name}}</span>
                <h3>{{$item->title}}</h3>
                <p class="card-text"> {{ Str::limit($item->post, 150) }}</p>
                <div class="row m-3">
                    <a href="/pertanyaan/{{$item->id}}" class="btn btn-primary btn-block">Detail</a>
                </div>
            </div>
        </div>
    </div>
@empty
    <h5 class="text-center">Belum ada pertanyaan ditambahkan</h5>
@endforelse
</div>

<a href="/ask" class="btn btn-dark btn-sm"><strong>Kembali</strong></a>

@endsection

