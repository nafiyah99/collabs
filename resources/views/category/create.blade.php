@extends('layout.master')

@section('title')
    Tambah Kategori
@endsection

@section('content')
    <form action="/ask" method="POST">
        @csrf
        <div class="form-group">
            <label>Nama Kategori</label>
            <input type="text" name="name" class="form-control" placeholder="Masukkan kategori baru">
        </div>
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Deskripsi</label>
            <textarea name="description" class="form-control" cols="30" rows="10"></textarea>
        </div>
        @error('description')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary px-5">Kirim</button>
    </form>
@endsection

