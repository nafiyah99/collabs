@extends('layout.master')

@section('title')
    Detail Pertanyaan
@endsection

@section('content')
    <div class="container">
        <div class="card">
            <img class="card-img-top" src="{{asset('/image/' . $post->asset)}}" alt="Card image cap">
            <div class="card-body">
                <h3>{{$post->title}}</h3>
                <p class="card-text"> {!!$post->post!!}</p>
            </div>
        </div>
    </div>

    
    <br><br><br>
    <div class="sharethis-inline-reaction-buttons"></div>
    <br><br>
    <div class="sharethis-inline-share-buttons"></div>
    <br><br><br>

        <h3>
            Komentar
        </h3>
    <hr>

    @forelse ($post->comment as $item)
    <div class="card" style="max-width: 50rem;">
        <div class="card-header bg-secondary">
            <strong>{{$item->user->name}}</strong>
            <a class="btn btn-secondary dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <a class="dropdown-item" href="/komentar/{{$item->id}}/edit">Edit</a>
                <a class="dropdown-item" type="submit" href="/komentar/{id}">Hapus</a>
            </div>
        </div>
        </div>
        <div class="card-body bg-light">
            <p class="card-text">{{$item->replies}}</p>
        </div>
        

    <br><br><br>
    @empty
        <h5 class="text-center">Belum ada komentar</h5>
    @endforelse

    <form action="/komentar/{{$post->id}}" method="POST">
        @csrf
        <div class="form-group">
            <textarea name="replies" class="form-control" placeholder="Tulis komentar!" id="" cols="30" rows="10"></textarea>
        </div>
        @error('replies')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <input type="submit" value="Kirim" class="btn btn-success">
    </form>

    <a href="/pertanyaan" class="btn btn-primary my-5 btn-block">Kembali</a>


@endsection

