@extends('layout.master')

@section('title')
    Edit Pertanyaan
@endsection

@section('content')
    <form action="/pertanyaan/{{$post->id}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Title</label>
            <input type="text" name="title" value="{{$post->title}}" class="form-control">
        </div>
        @error('title')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Pertanyaan</label>
            <textarea name="post" class="form-control" cols="30" rows="10">{{$post->post}}</textarea>
        </div>
        @error('post')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Gambar</label>
            <input type="file" name="asset" class="form-control">
        </div>
        @error('asset')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Category</label>
            <select name="category_id" class="form-control" id="">
                <option value="">- Pilih Kategori -</option>

                @forelse ($category as $item)

                @if ($item->id === $post->category_id)
                    <option value="{{$item->id}}" selected>{{$item->name}}</option>
                @else
                    <option value="{{$item->id}}">{{$item->name}}</option>
                @endif
                @empty
                    <option value="">Kategori belum ditambahkan</option>

                @endforelse
            </select>
        </div>
        @error('category_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary px-5">Kirim</button>
    </form>

@push('script')
<script src="https://cdn.tiny.cloud/1/tjhfeqnhozuyyyqjt3fdvud789dx43g1ep4tfy5f6g61cmcm/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount checklist mediaembed casechange export formatpainter pageembed linkchecker a11ychecker tinymcespellchecker permanentpen powerpaste advtable advcode editimage tinycomments tableofcontents footnotes mergetags autocorrect typography inlinecss',
      toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table mergetags | addcomment showcomments | spellcheckdialog a11ycheck typography | align lineheight | checklist numlist bullist indent outdent | emoticons charmap | removeformat',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
      mergetags_list: [
        { value: 'First.Name', title: 'First Name' },
        { value: 'Email', title: 'Email' },
      ]
    });
</script>
@endpush
@endsection

