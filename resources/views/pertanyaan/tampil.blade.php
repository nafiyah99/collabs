@extends('layout.master')

@section('title')
    Daftar Pertanyaan
@endsection

@section('content')
    <a href="/pertanyaan/create" class="btn btn-primary my-3">+ Tambah Pertanyaan</a>

    <div class="row">
        @forelse ($post as $item)
        <div class="col-4">
            <div class="card">
                <img class="card-img-top" src="{{asset('/image/' . $item->asset)}}" height="350px" alt="Card image cap">
                <div class="card-body">
                <span class="badge badge-warning">{{$item->category->name}}</span>
                    <h3>{{$item->title}}</h3>
                    <p class="card-text"> {!! Str::limit($item->post, 150) !!}</p>
                    <div class="row m-3">
                    <a href="/pertanyaan/{{$item->id}}" class="btn btn-primary btn-block">Detail</a>
                    </div>
                    <div class="row m-3">
                        <div class="col-">
                            <form action="/pertanyaan/{{$item->id}}" method="POST">
                                @csrf
                                @method('delete')
                                <input type="submit" class="btn btn-outline-danger btn-block ml-3" value="Hapus">
                            </form>
                        </div>
                        <div class="col-2"></div>
                        <div class="col-">
                            <a href="/pertanyaan/{{$item->id}}/edit" class="btn btn-outline-warning btn-block ml-4">Edit</a>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

        @empty
            <h4><br>Belum ada pertanyaan ditambahkan</h4>
        @endforelse
        
    </div>

@endsection

