    <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="/home">
            <i class="icon-grid menu-icon"></i>
            <span class="menu-title">Dashboard</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/ask">
            <i class="icon-paper menu-icon"></i>
            <span class="menu-title">Category</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
            <i class="icon-layout menu-icon"></i>
            <span class="menu-title">Pertanyaan</span>
            <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="/pertanyaan/create">Tambah Pertanyaan</a></li>
                <li class="nav-item"> <a class="nav-link" href="/pertanyaan">List Pertanyaan</a></li>
            </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#form-elements" aria-expanded="false" aria-controls="form-elements">
            <i class="ti-user menu-icon"></i>
            <span class="menu-title">My Profile</span>
            <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="form-elements">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item"><a class="nav-link">{{ Auth::user()->profile->name }}</a></li>
                <li class="nav-item"><a class="nav-link">{{ Auth::user()->profile->age }}</a></li>
                <li class="nav-item"><a class="nav-link">{{ Auth::user()->profile->address }}</a></li>
                <li class="nav-item"><a class="nav-link">{{ Auth::user()->profile->bio }}</a></li>
            </ul>
            </div>
        </li>
        </ul>
    </nav>