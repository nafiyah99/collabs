<?php

use App\Http\Controllers\AskController;
use App\Http\Controllers\CommentarController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\PertanyaanController;  
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layout.index');
});

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Auth::routes();

// Route::get('/master', function () {
//     return view('layout.master');
// });

Route::middleware(['auth'])->group(function () {
    
Route::get('/home', [AskController::class, 'home']);


// CRUD Kategori

// C
// Route mengarah ke form tambah kategori
Route::get('/ask/create', [AskController::class, 'create']);

// Route menyimpan inputan ke dalam table pada database
Route::post('/ask', [AskController::class, 'store']);

// R
// Route untuk halaman yang menampilkan semua data pada table category
Route::get('/ask', [AskController::class, 'index']);

// Route detail category berdasarkan id
Route::get('/ask/{id}', [AskController::class, 'show']);

// U
// Route ke halaman edit kategori
Route::get('/ask/{id}/edit', [AskController::class, 'edit']);

// Route untuk edit data berdasarkan id
Route::put('/ask/{id}', [AskController::class, 'update']);

//D
// Route untuk menghapus data berdasarkan id
Route::delete('/ask/{id}', [AskController::class, 'destroy']);

//CRUD Profile


Route::get('/profile', [ProfileController::class, 'profile']);
Route::put('/profile/{id}', [ProfileController::class, 'update']);

// CRUD Pertanyaan
Route::resource('pertanyaan', PertanyaanController::class);

// Komentar
Route::post('/komentar/{post_id}', [CommentarController::class, 'store']);

Route::get('/komentar/{id}/edit', [CommentarController::class, 'edit']);
Route::delete('/komentar/{id}', [CommentarController::class, 'destroy']);
Route::put('/komentar/{id}', [CommentarController::class, 'update']);
});
